import Plugin from "@ckeditor/ckeditor5-core/src/plugin";
import Command from "@ckeditor/ckeditor5-core/src/command";

import {
  toWidget,
  viewToModelPositionOutsideModelElement,
} from "@ckeditor/ckeditor5-widget/src/utils";
import Widget from "@ckeditor/ckeditor5-widget/src/widget";

class PlaceholderCommand extends Command {
  execute({ value }) {
    const editor = this.editor;
    const selection = editor.model.document.selection;

    editor.model.change((writer) => {
      // Create a <placeholder> elment with the "name" attribute (and all the selection attributes)...
      const placeholder = writer.createElement("placeholder", {
        ...Object.fromEntries(selection.getAttributes()),
        name: value,
        contenteditable: false,
      });

      // ...and insert it into the document.
      editor.model.insertContent(placeholder);

      // Put the selection on the inserted element.
      writer.setSelection(placeholder, "on");
    });
  }

  refresh() {
    const model = this.editor.model;
    const selection = model.document.selection;

    const isAllowed = model.schema.checkChild(
      selection.focus.parent,
      "placeholder"
    );
    this.isEnabled = isAllowed;
  }
}

class PlaceholderUI extends Plugin {
  // not needed
  init() {}
}

class PlaceholderEditing extends Plugin {
  static get requires() {
    return [Widget];
  }

  init() {
    this._defineSchema();
    this._defineConverters();

    this.editor.commands.add(
      "placeholder",
      new PlaceholderCommand(this.editor)
    );

    this.editor.editing.mapper.on(
      "viewToModelPosition",
      viewToModelPositionOutsideModelElement(this.editor.model, (viewElement) =>
        viewElement.hasClass("placeholder")
      )
    );
  }

  _defineSchema() {
    const schema = this.editor.model.schema;

    schema.register("placeholder", {
      // Allow wherever text is allowed:
      allowWhere: "$text",

      // The placeholder will act as an inline node:
      isInline: true,

      // The inline widget is self-contained so it cannot be split by the caret and can be selected:
      isObject: true,

      // The inline widget can have the same attributes as text (for example linkHref, bold).
      allowAttributesOf: "$text",

      // The placeholder can have many types, like date, name, surname, etc:
      allowAttributes: ["name"],
    });
  }

  _defineConverters() {
    const conversion = this.editor.conversion;

    conversion.for("upcast").elementToElement({
      view: {
        name: "span",
        classes: ["placeholder"],
      },
      model: (viewElement, { writer: modelWriter }) => {
        // Extract the "name" from "{name}".
        const name = viewElement.getChild(0).data.slice(1, -1);

        return modelWriter.createElement("placeholder", { name });
      },
    });

    conversion.for("editingDowncast").elementToElement({
      model: "placeholder",
      view: (modelItem, { writer: viewWriter }) => {
        const widgetElement = createPlaceholderView(modelItem, viewWriter);

        return toWidget(widgetElement, viewWriter);
      },
    });

    conversion.for("dataDowncast").elementToElement({
      model: "placeholder",
      view: (modelItem, { writer: viewWriter }) =>
        createPlaceholderView(modelItem, viewWriter),
    });

    // Helper method for both downcast converters.
    function createPlaceholderView(modelItem, viewWriter) {
      const name = modelItem.getAttribute("name");

      const placeholderView = viewWriter.createContainerElement(
        "span",
        { class: "placeholder" },
        {
          isAllowedInsideAttributeElement: true,
        }
      );

      // insert the placeholder name (as a text)
      const innerText = viewWriter.createText(name);
      viewWriter.insert(
        viewWriter.createPositionAt(placeholderView, 0),
        innerText
      );

      return placeholderView;
    }
  }
}

export class Placeholder extends Plugin {
  static get requires() {
    return [PlaceholderEditing, PlaceholderUI];
  }
}

class SectionCommand extends Command {
  execute() {
    const editor = this.editor;

    editor.model.change((writer) => {
      const section = writer.createElement("section");

      // ...and insert it into the document.
      editor.model.insertContent(section);

      writer.setSelection(section, "on");
    });
  }
}

export class Section extends Plugin {
  init() {
    const editor = this.editor;
    // section -> toc map
    const trackedSections = new Map();

    editor.model.schema.register("section", {
      allowIn: "$root",
      allowContentOf: "$root",
    });

    editor.conversion.for("upcast").elementToElement({
      model: "section",
      view: "section",
    });

    editor.conversion.for("editingDowncast").elementToElement({
      model: "section",
      view: (modelElement, { writer: viewWriter }) => {
        const section = viewWriter.createContainerElement("section");

        const toc = viewWriter.createUIElement(
          "div",
          {
            class: "toc",
            contenteditable: "false", // Make it invisible for the selection, to not block up/down arrow keys.
          },
          function (domDocument) {
            const domElement = this.toDomElement(domDocument);

            domElement.innerHTML =
              "<div>Section TOC...</div><button>toggle</button>";

            const [div, button] = domElement.childNodes;

            button.addEventListener("click", (evt) => {
              evt.stopPropagation();
              evt.preventDefault();

              div.classList.toggle("hidden");
            });

            return domElement;
          }
        );

        trackedSections.set(section, toc);

        return section;
      },
    });

    editor.conversion.for("dataDowncast").elementToElement({
      model: "section",
      view: "section",
    });

    editor.editing.view.document.registerPostFixer((writer) => {
      // Cleanup section removed from the document first.
      for (const section of trackedSections.keys()) {
        if (!section.isAttached()) {
          console.log("Stopping tracking a section");

          trackedSections.delete(section);
        }
      }

      // Ensure position at the beginning of a section.
      for (const [section, toc] of trackedSections) {
        if (section.getChild(0) !== toc) {
          console.log("Repositioning a TOC");
          writer.insert(writer.createPositionAt(section, 0), toc);
        }
      }
    });

    this.editor.commands.add("section", new SectionCommand(this.editor));
  }
}
